import * as React from 'react';
import ReactSwitch from 'react-switch';
import { colors } from '../../utils/variables';

export interface SwitchProps {
  offColor?: string;
  onColor?: string;
  offHandleColor?: string;
  onHandleColor?: string;
}

const configColor = (color: string | undefined): string | undefined => {
  try {
    return colors[color || ''] || color;
  } catch {
    return '';
  }
};

const Switch = (props: any) => {
  const { offColor, onColor, offHandleColor, onHandleColor }: SwitchProps = props;

  const modOffColor: string | undefined = configColor(offColor);
  const modOnColor: string | undefined = configColor(onColor);
  const modOffHandlerColor: string | undefined = configColor(offHandleColor);
  const modOnHandlerColor: string | undefined = configColor(onHandleColor);

  return (
    <ReactSwitch
      {...props}
      offColor={modOffColor}
      onColor={modOnColor}
      offHandleColor={modOffHandlerColor}
      onHandleColor={modOnHandlerColor}
    />
  );
};

export default Switch;
