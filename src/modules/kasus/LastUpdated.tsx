import * as React from 'react';
import { Box, Text } from 'components/design-system';

interface LastUpdatedProps {
  updatedAt: string;
}

const LastUpdated: React.FC<LastUpdatedProps> = ({ updatedAt }) => {
  return (
    <Box>
      <Text as="h5" variant={200} color="accents04" fontWeight={400}>
        Pembaruan Terakhir
      </Text>
      <Text variant={200} color="accents07" fontFamily="IBM Plex Mono">
        {updatedAt}
      </Text>
    </Box>
  );
};

export default LastUpdated;
